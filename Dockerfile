FROM metabase/metabase

COPY ./entrypoint.sh /app/entrypoint.sh

RUN apk add --update python3 \
    && pip3 install -U s3conf==0.6.0 \
    && rm -rf /tmp/* /var/cache/apk/* \
    && chmod 755 /app/entrypoint.sh 

EXPOSE 3000
WORKDIR /app
ENTRYPOINT ["/app/entrypoint.sh"]
