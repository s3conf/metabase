#!/usr/bin/env bash

set -e
export $(s3conf env -m)
exec "/app/run_metabase.sh"
